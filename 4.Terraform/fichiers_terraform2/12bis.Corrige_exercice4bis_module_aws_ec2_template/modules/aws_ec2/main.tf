data "aws_ami" "debian" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-11-amd64-20220911-1135"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["136693071363"] # debian official owner
}
resource "aws_instance" "myfirstinstance" {
  ami= data.aws_ami.debian.id
  instance_type     = "t2.micro"
  #region définie dans les credentials du provider
  key_name="awsparis"
  tags = {
    Owner="myname"
    Name="helloworld"
    }
  root_block_device {
  volume_size="30"
  volume_type="gp3"
  }

  #security_groups=["mysecuritygroup1","mysg2"]
  #monitoring=true
  #disable_api_stop=true
  #disable_api_termination=true
  #launch_template{
    #id="myid" #id conflicts with name   
    #name="mytemplate"
    #version="myversion"/$Latest/$Default
    #}

  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = self.public_ip
    private_key = file(var.ssh_key)
  }
  provisioner "remote-exec" {
    host        = self.public_ip
    inline = [
      "echo test",
    ]
  }
  #provisioner "local-exec" {
  #    command = "ansible-playbook -u admin -i '${self.public_ip},' playbook.yml"
  #  }
}
