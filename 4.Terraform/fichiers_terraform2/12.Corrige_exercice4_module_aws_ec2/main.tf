variable "ssh_user" {}
variable "ssh_key" {}

module "aws_ec2"{
    source="./modules/aws_ec2"
    ssh_user=var.ssh_user
    ssh_key=var.ssh_key
}


output "ec2_ip" {
  value = module.aws_ec2.ec2_ip
}

