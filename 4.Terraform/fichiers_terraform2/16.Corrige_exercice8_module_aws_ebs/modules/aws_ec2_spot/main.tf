data "aws_ami" "debian" {
  most_recent = true

  filter {
    name   = "name"
    values = ["debian-11-amd64-20220911-1135"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["136693071363"] # debian official owner
}
resource "aws_spot_instance_request" "myfirstinstance" {
  ami= data.aws_ami.debian.id
  instance_type     = "t2.micro"
  key_name="awsparis"
  tags = { #les tags ne fonctionnent pas sur les spot instances cf https://github.com/hashicorp/terraform/issues/3263 pour des workarounds
    Name="myfirstspotinstance"
    Owner="myname"}
  tags_all = {
    Name="myfirstspotinstance"
    Owner="myname"}
  wait_for_fulfillment = true #important sinon ca plante pour recuperer la public_ip
  root_block_device {
  volume_size="30"
  volume_type="gp3"
  }

  #security_groups=["mysecuritygroup1","mysg2"]
  #monitoring=true
  #disable_api_stop=true
  #disable_api_termination=true
  #launch_template{
    #id="myid" #id conflicts with name   
    #name="mytemplate"
    #version="myversion"/$Latest/$Default
    #}

  connection {
    type        = "ssh"
    user        = var.ssh_user
    host        = self.public_ip
    private_key = file(var.ssh_key)
  }
  provisioner "remote-exec" {
    inline = [
      "echo test",
    ]
  }
  #provisioner "local-exec" {
  #    command = "ansible-playbook -u admin -i '${self.public_ip},' playbook.yml"
  #  }
}
