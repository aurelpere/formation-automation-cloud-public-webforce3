variable "ssh_user" {}
variable "ssh_key" {}

module "aws_ec2_spot"{
    source="./modules/aws_ec2_spot"
    ssh_user=var.ssh_user
    ssh_key=var.ssh_key
}


output "ec2_ip" {
  value = module.aws_ec2_spot.ec2_ip
}

