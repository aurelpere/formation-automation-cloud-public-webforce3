provider "docker" {
  host = "tcp://${var.ssh_host}:2375"
}

resource "docker_network" "mydockernetwork" {
    name = "mynet" #note sur le stateful : si on change le nom terraform supprimera l'ancienne ressource
#    driver="bridge"
#    ipam_config {
#        subnet="172.22.0.0/24"
#    }
}

resource "docker_image" "nginx" {
  name = "nginx:latest"
}
resource "docker_container" "nginx" {
  image = docker_image.nginx.latest
  name  = "enginecks"
  ports {
    internal = 80
    external = 80
  }
 networks_advanced {
    name=docker_network.mydockernetwork.name
  }
}

output "ip_docker" {
 value= docker_container.nginx.ip_address
}

