variable "ami_id" {
  type    = string
  default = "ami-09a41e26df464c548"
}

variable "aws_access_key" {
  type    = string
  default = env("AWS_ID")
}

variable "aws_secret_key" {
  type    = string
  default = env("AWS_SECRET")
}

variable "app_name" {
  type    = string
  default = "helloworld"
}

packer {
  required_plugins {
    amazon = {
      version = ">= 1.1.1"
      source = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "example" {
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  ami_name      = "PACKER-DEMO-${var.app_name}"
  instance_type = "t2.micro"
  region        = "eu-west-3"
 source_ami_filter {
       filters= {
       virtualization-type = "hvm"
       name="debian-11-amd64-20220911-1135"
       root-device-type= "ebs"
       }
       owners= ["136693071363"]
       most_recent= true
    }
  ssh_username  = "admin"
  tags = {
    Env  = "DEMO"
    Name = "PACKER-DEMO-${var.app_name}"
  }
}

build {
  name= "learn-packer"
  sources = ["source.amazon-ebs.example"]

  provisioner "file" {
    source = "./script.py"
    destination = "/tmp/script.py"
  }

  provisioner "shell" {
    inline = ["python3 /tmp/script.py"]
  }

  #provisioner "shell" {
  #  script = "script/script.sh"
  #}
  #provisioner "ansible-local" {
  #  playbook_file = "./playbook.yml"
  #	role_paths = ["."]
  # extra_arguments= [ 
  #	  "-vvv",
  #	  "--extra-vars",
  #	  "'ansible_python_interpreter=/usr/bin/python3'"
  #	]

  post-processor "shell-local" {
    inline = ["echo foo"]
  }
}
