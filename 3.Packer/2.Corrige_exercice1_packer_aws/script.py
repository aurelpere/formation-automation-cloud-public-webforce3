#! /usr/bin/python3
# -*- coding:Utf8 -*-
import subprocess 
for cmd in ["sudo fallocate -l 4G /swapfile ",
            "sudo chmod 600 /swapfile",
            "sudo mkswap /swapfile",
            "sudo swapon /swapfile",
            "sudo swapon --show",
            "sudo cp /etc/fstab /etc/fstab.bak",
            "echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab"]:
    child=subprocess.Popen(cmd, shell=True,
                     stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    streamdata = child.communicate()[0]
    if streamdata:
        print(streamdata)
    streamerror= child.communicate()[1]
    if streamerror:    
        print(streamerror)
    #to replace set -e
    if child.returncode > 0:
        print(f"Command '{cmd}' was not successful")
        print(f"----")
        print(f"Interrupting")
        print(f"----")
        break
    else:
        print (f"Command '{cmd}' was successful")
