#!/bin/bash

if ! rpm -q httpd; then
   echo "Apache HTTPD is not Installed"
   echo
   echo "Installing the latest version"
   echo
   sudo yum install httpd -y > /dev/null
   if [ $? -eq 0 ]; then
      echo "Apache HTTPD Installed"
      sudo systemctl start httpd
   fi
fi
