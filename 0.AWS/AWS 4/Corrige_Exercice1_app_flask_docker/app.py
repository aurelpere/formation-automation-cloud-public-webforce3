#!/usr/bin/python3
# coding: utf-8
"""
this is app.py for flask-tutorial (sondage example)
"""
from flask import Flask, request
from handlers.routes import configure_routes


# create the Flask app
app = Flask(__name__)
app.config['SECRET_KEY'] = '36b788e06d0c84d524eb881879ac7e2f66352b6480717bb9'
configure_routes(app)



if __name__ == '__main__':
    # run app in debug mode on port 5000
    app.run(debug=True, host="0.0.0.0", port=5000)
