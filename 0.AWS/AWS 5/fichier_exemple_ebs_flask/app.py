"""
this is a demo for elastic beanstalk flask app
"""
from flask import Flask
from flask import jsonify

application = Flask(__name__)


@application.route("/")
def hello():
    """Return a friendly HTTP greeting."""
    print("I am inside hello world")
    return "Continuous Delivery Demo"


@application.route("/echo/<name>")
def echo(name):
    "echo function"
    print(f"This was placed in the url: new-{name}")
    val = {"new-name": name}
    return jsonify(val)


if __name__ == "__main__":
    application.run(debug=True,host="0.0.0.0", port=5000)
