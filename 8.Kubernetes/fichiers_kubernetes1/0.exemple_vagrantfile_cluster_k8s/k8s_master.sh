#!/bin/bash
set -e

#configuring /etc/hosts
echo "192.168.56.102 kubnode" | sudo tee -a /etc/hosts > /dev/null
echo "192.168.56.101 kubmaster" | sudo tee -a /etc/hosts > /dev/null

#desactivating swap
sudo swapoff -a
#sudo sed -i 's/\/swap.img	none	swap	sw	0	0/#\/swap.img	none	swap	sw	0	0/g' /etc/fstab
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo mount -a

#kernel routing parameters
#activer l'ip forwarding
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
#bridged packets will traverse iptables rules
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
#bridged packets will traverse iptables rules
echo "net.bridge.bridge-nf-call-ip6tables = 1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
sudo sysctl --system

#installing packages for containerd and configuring containerd
sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common containerd.io
sudo containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml

#installing cni plugin for containerd
wget https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz
sudo mkdir -p /opt/cni/bin
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.1.1.tgz
#containerd config files: https://kubernetes.io/docs/tasks/administer-cluster/migrating-from-dockershim/troubleshooting-cni-plugin-related-errors/
#for /etc/cni/net.d config files
cat << EOF | sudo tee /etc/cni/net.d/10-containerd-net.conflist
{
 "cniVersion": "1.0.0",
 "name": "containerd-net",
 "plugins": [
   {
     "type": "bridge",
     "bridge": "cni0",
     "isGateway": true,
     "ipMasq": true,
     "promiscMode": true,
     "ipam": {
       "type": "host-local",
       "ranges": [
         [{
           "subnet": "10.88.0.0/16"
         }],
         [{
           "subnet": "2001:db8:4860::/64"
         }]
       ],
       "routes": [
         { "dst": "0.0.0.0/0" },
         { "dst": "::/0" }
       ]
     }
   },
   {
     "type": "portmap",
     "capabilities": {"portMappings": true}
   }
 ]
}
EOF


#restarting containerd
sudo systemctl restart containerd
sudo systemctl enable containerd

#loading overlay and br_netfilter kernel modules
echo "overlay" | sudo tee -a /etc/modules-load.d/containerd.conf > /dev/null
echo "br_netfilter" | sudo tee -a /etc/modules-load.d/containerd.conf > /dev/null
sudo modprobe overlay
sudo modprobe br_netfilter

#installing kubernetes packages
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update -y && sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni

#verifier que le cgroup driver est mis en place dans la config de kubelet: /etc/kubernetes/kubelet.conf
#apiVersion: kubelet.config.k8s.io/v1beta1
#kind: KubeletConfiguration
#...
#cgroupDriver: systemd

#initializing kubernetes
#initialisation de kubernetes
sudo kubeadm init --apiserver-advertise-address=192.168.56.101 --node-name $HOSTNAME --pod-network-cidr=192.168.0.0/16 --control-plane-endpoint=kubmaster
sudo mkdir -p /home/vagrant/.kube
sudo cp -i /etc/kubernetes/admin.conf /home/vagrant/.kube/config
sudo chown $(id -u vagrant):$(id -g vagrant) /home/vagrant/.kube/config

#calico setup
# installation du cni - edit 10.244.0.0/16 cidr ip block??
sudo -i -u vagrant bash << EOF
kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.24.1/manifests/tigera-operator.yaml  
curl https://raw.githubusercontent.com/projectcalico/calico/v3.24.1/manifests/custom-resources.yaml -O
kubectl create -f custom-resources.yaml
#get pods
kubectl get pods --all-namespaces
echo "source <(kubectl completion bash)" >> /home/vagrant/.bashrc
EOF
#kubectl get nodes
#kubectl cluster-info dump #si soucis sur les pods
#systemctl restart kubelet #si soucis sur les pods
#commande pour relancer calico ou flannel (supprimer les pods): 

