#!/bin/bash
set -e
sudo -i -u vagrant bash << EOF
set -e
echo "192.168.56.102 kubnode" | sudo tee -a /etc/hosts > /dev/null
echo "192.168.56.101 kubmaster" | sudo tee -a /etc/hosts > /dev/null

#desactivating swap
sudo swapoff -a
#sudo sed -i 's/\/swap.img	none	swap	sw	0	0/#\/swap.img	none	swap	sw	0	0/g' /etc/fstab
sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
sudo mount -a

#kernel routing parameters
#activer l'ip forwarding
echo "net.ipv4.ip_forward=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
#bridged packets will traverse iptables rules
echo "net.bridge.bridge-nf-call-iptables=1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
#bridged packets will traverse iptables rules
echo "net.bridge.bridge-nf-call-ip6tables = 1" | sudo tee -a /etc/sysctl.d/kubernetes.conf > /dev/null 
sudo sysctl --system

#installing packages for containerd and configuring containerd
sudo apt-get update && sudo apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common containerd.io
sudo containerd config default | sudo tee /etc/containerd/config.toml >/dev/null 2>&1
sudo sed -i 's/SystemdCgroup \= false/SystemdCgroup \= true/g' /etc/containerd/config.toml




#installing cni plugin for containerd
wget https://github.com/containernetworking/plugins/releases/download/v1.1.1/cni-plugins-linux-amd64-v1.1.1.tgz
sudo mkdir -p /opt/cni/bin
sudo tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.1.1.tgz

#restarting containerd
sudo systemctl restart containerd
sudo systemctl enable containerd

#loading overlay and br_netfilter kernel modules
echo "overlay" | sudo tee -a /etc/modules-load.d/containerd.conf > /dev/null
echo "br_netfilter" | sudo tee -a /etc/modules-load.d/containerd.conf > /dev/null
sudo modprobe overlay
sudo modprobe br_netfilter

#installing kubernetes packages
sudo curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update -y && sudo apt-get install -y kubelet kubeadm kubectl kubernetes-cni

#sudo kubeadm join kubmaster:6443 --token upzc6d.dna3fo36e30bqvk2 --discovery-token-ca-cert-hash sha256:26fe8414d310720794bbf0093b782da4f409b8f8acd6bf047921b4ecd3fb0f96
